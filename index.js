/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication. (use return statement)

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division. (use return statement)

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided radius.
			-a number should be provided as an argument.
			-formula: area = pi * (radius raised to two)
			-look up the use of the exponent operator. (ES6 update)
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation. (use return statement)

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

		Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-formula for average: num1 + num2 + num3 + ... divide by total numbers
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation. (use return statement) 

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/




						/*SOLUTIONS*/
/*PROBLEM I*/

		function addCalc(add1, add2){
			let addition = add1 + add2
			console.log("The sum of 100 and 50 is?");
			console.log(addition);
		};

		addCalc(100, 50);

		function subCalc(subt1, subt2){
			let subtraction = subt1 - subt2
			console.log("The difference of 100 and 50 is?");
			console.log(subtraction);
			// console.log(typeof subtraction)
		};
		subCalc(100,50);

/*PROBLEM II*/

		function multCalc(mult1, mult2){
			let multiplication = mult1 * mult2
			return multiplication 
			
			
		};
		let multiplication = multCalc("100", "50");
		console.log("The product of 100 and 50 is?");
		console.log(multiplication);

		function divCalc(divi1, divi2){
			let division = divi1 / divi2
			return division 
			
			
		};
		let division = divCalc("100", "50");
		console.log("The quotient of 100 and 50 is?");
		console.log(division);

/*PROBLEM III*/


		function areaCircle(pi, radius){
			let area = pi * (radius **2) //or pi * (radius * radius)
			return area
		};

		let area = areaCircle("3.1416", "30")
		console.log("The area of a circle with radius of 30 is?");
		console.log(area);

/*PROBLEM IV*/

		function averageOfGrades(grd1, grd2, grd3, grd4, grd5 ){
			let average = (grd1 + grd2 + grd3 + grd4 + grd5) / 5 
			return average
		};

		let average = averageOfGrades(95, 88.2, 92, 91, 87.5)
			console.log("The average of the given grades are?")
			console.log(average);


/*PROBLEM V*/

		function passingScore(grade){
			let isPassingScore = grade >= 75
			return isPassingScore
		};
		let isPassingScore = passingScore("90.7");
		console.log("Is your score passing?")
		console.log(isPassingScore);

